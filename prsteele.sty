\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{prsteele}

\RequirePackage{amsmath}
\RequirePackage{amsthm}
\RequirePackage{amssymb}
\RequirePackage{ifthen}
\usepackage{bbold}

% Proofs and lemmas
\theoremstyle{plain}
\newtheorem{prop}{Prop}
\newtheorem*{prop*}{Prop}
\newtheorem{lem}{Lemma}
\newtheorem{lemma}{Lemma}
\newtheorem*{lem*}{Lemma}
\newtheorem*{lemma*}{Lemma}
\newtheorem{theorem}{Theorem}
\newtheorem*{theorem*}{Theorem}
\newtheorem{cor}{Corollary}
\newtheorem{corollary}{Corollary}
\newtheorem*{cor*}{Corollary}
\newtheorem*{corollary*}{Corollary}
\newtheorem*{remark}{Remark}
\newenvironment{optLem}[1]{\vspace{1em}\noindent{\bf Lemma
    #1.}\begingroup\it }{\endgroup}

\theoremstyle{definition}

\newtheorem{defn}{Definition}

% Macros for matching delimiters, with and without resizing. Unstarred
% commands resize, starred commands do not.

\newcommand{\prsteele@parennostar}[1]{\left(#1\right)}
\newcommand{\prsteele@parenstar}[1]{(#1)}
\newcommand{\paren}{%
  \@ifstar{\prsteele@parenstar}{\prsteele@parennostar}%
}

\newcommand{\prsteele@cbracenostar}[1]{\left\{#1\right\}}
\newcommand{\prsteele@cbracestar}[1]{\{#1\}}
\newcommand{\cbrace}{%
  \@ifstar{\prsteele@cbracestar}{\prsteele@cbracenostar}%
}

\newcommand{\prsteele@sbracenostar}[1]{\left[#1\right]}
\newcommand{\prsteele@sbracestar}[1]{[#1]}
\newcommand{\sbrace}{%
  \@ifstar{\prsteele@sbracestar}{\prsteele@sbracenostar}%
}

\newcommand{\prsteele@floornostar}[1]{\left\lfloor #1 \right\rfloor}
\newcommand{\prsteele@floorstar}[1]{\lfloor #1 \rfloor}
\newcommand{\floor}{%
  \@ifstar{\prsteele@floorstar}{\prsteele@floornostar}%
}

\newcommand{\prsteele@ceilnostar}[1]{\left\lceil #1 \right\rceil}
\newcommand{\prsteele@ceilstar}[1]{\lceil #1 \rceil}
\newcommand{\ceil}{%
  \@ifstar{\prsteele@ceilstar}{\prsteele@ceilnostar}%
}

\newcommand{\prsteele@absnostar}[1]{\left| #1 \right|}
\newcommand{\prsteele@absstar}[1]{|#1|}
\newcommand{\abs}{%
  \@ifstar{\prsteele@absstar}{\prsteele@absnostar}%
}

\newcommand{\prsteele@honostar}[1]{\left(\left. #1 \right]\right.}
\newcommand{\prsteele@hostar}[1]{( #1 ]}
\newcommand{\halfopen}{%
  \@ifstar{\prsteele@hostar}{\prsteele@honostar}
}

\newcommand{\prsteele@hcnostar}[1]{\left[\left. #1 \right)\right.}
\newcommand{\prsteele@hcstar}[1]{[ #1 )}
\newcommand{\halfclosed}{%
  \@ifstar{\prsteele@hcstar}{\prsteele@hcnostar}
}

\newcommand{\prsteele@innernostar}[2]{\left\langle #1, #2 \right\rangle}
\newcommand{\prsteele@innerstar}[2]{\langle #1, #2 \rangle}
\newcommand{\inner}{%
  \@ifstar{\prsteele@innerstar}{\prsteele@innernostar}
}

\newcommand{\prsteele@cardnostar}[1]{\left| #1 \right|}
\newcommand{\prsteele@cardstar}[1]{| #1 |}
\newcommand{\card}{%
  \@ifstar{\prsteele@cardstar}{\prsteele@cardnostar}
}

% Nice definitions of some common operators
\DeclareMathOperator{\dom}{\bf dom}
\DeclareMathOperator{\vspan}{\bf span}
\DeclareMathOperator{\rank}{\bf rank}
\DeclareMathOperator{\prob}{\mathbb{P}}
\DeclareMathOperator{\sign}{\bf sign}
\DeclareMathOperator{\range}{\bf range}
\DeclareMathOperator{\trace}{\bf tr}
\DeclareMathOperator{\nullsp}{\bf null}
\DeclareMathOperator{\cls}{\bf cls}
\DeclareMathOperator{\interior}{\bf int}
\DeclareMathOperator{\expectation}{\mathbb{E}}
\DeclareMathOperator{\var}{Var}
\DeclareMathOperator{\cov}{Cov}
\DeclareMathOperator{\ind}{\ensuremath{\mathrm{1}}}
\DeclareMathOperator{\Diag}{Diag}
\DeclareMathOperator{\diag}{diag}

% Nice set notation, e.g. {a | b}. \st and \given should _only_ be
% used inside a \set command.
%
% We accomplish this by storing the height of the content of the nth
% nested set in \prs@setheightn. The \st and \given commands then
% create delimiters of the appropriate size.
%
% \newcommand{\prs@makesetheight}[1]{\csname prs@setheight#1\endcsname}
% \newcounter{prs@setdepth}
% \setcounter{prs@setdepth}{0}
% \newcommand{\set}[1]{%
%   \addtocounter{prs@setdepth}{1}
%   \@ifdefinable{\prs@makesetheight{\value{prs@setdepth}}}{%
%     \newlength{\prs@makesetheight{\value{prs@setdepth}}}
%   }
%   \setlength{\prs@makesetheight{\value{prs@setdepth}}}{1in}
% }

\newlength{\prs@setheight}

\newcommand{\set}[1]{%
  \mathchoice{
  \setbox0=\vbox{\vphantom{\ensuremath{\displaystyle\cbrace{#1}}}}
  \setlength{\prs@setheight}{\ht0}
  \cbrace{#1}
}{%
  \setbox0=\vbox{\vphantom{\ensuremath{\cbrace{#1}}}}
  \setlength{\prs@setheight}{\ht0}
  \cbrace{#1}
}{%
  \setbox0=\vbox{\vphantom{\ensuremath{\cbrace{#1}}}}
  \setlength{\prs@setheight}{\ht0}
  \cbrace{#1}
}{%
  \setbox0=\vbox{\vphantom{\ensuremath{\cbrace{#1}}}}
  \setlength{\prs@setheight}{\ht0}
  \cbrace{#1}
}}

\newcommand{\st}{\left|\rule{0pt}{\prs@setheight}\right.}

% Blackboard bold letters
\newcommand{\real}{\ensuremath{\mathbb{R}}}
\newcommand{\complex}{\ensuremath{\mathbb{C}}}
\newcommand{\integer}{\ensuremath{\mathbb{Z}}}
\renewcommand{\natural}{\ensuremath{\mathbb{N}}}
\newcommand{\rational}{\ensuremath{\mathbb{Q}}}
\newcommand{\irrational}{\ensuremath{\real \setminus \rational}}
\newcommand{\symmetric}{\ensuremath{\mathbb{M}}}

% Sets
\newcommand{\union}{\cup}
\newcommand{\Union}{\bigcup}
\newcommand{\intersect}{\cap}
\newcommand{\Intersect}{\bigcap}

% Display style sums
\newcommand{\Sum}{\displaystyle \sum}

% Miscellaneous
\newcommand{\tr}{\ensuremath{^\text{T}}}
\newcommand{\invtr}{\ensuremath{^{-\text{T}}}}
\newcommand{\inv}{\ensuremath{^{-1}}}
\newcommand{\norm}[1]{\ensuremath{\left\lVert #1 \right\rVert}}
\newcommand{\bigO}{\cO}

%% Miscellaneous bold and caligraphic constants
\newcommand{\bzero}{{\textbf{0}}}
\newcommand{\bone}{{\textbf{1}}}
\newcommand{\balpha}{{\ensuremath{\pmb{\alpha}}}}
\newcommand{\bbeta}{{\ensuremath{\pmb{\beta}}}}
\newcommand{\bdelta}{{\ensuremath{\pmb{\delta}}}}
\newcommand{\bgamma}{{\ensuremath{\pmb{\gamma}}}}
\newcommand{\bepsilon}{{\ensuremath{\pmb{\epsilon}}}}
\newcommand{\btheta}{{\ensuremath{\pmb{\theta}}}}
\newcommand{\ba}{\textbf{a}}
\newcommand{\bb}{\textbf{b}}
\newcommand{\bc}{\textbf{c}}
\newcommand{\bd}{\textbf{d}}
\newcommand{\be}{\textbf{e}}
\newcommand{\bff}{\textbf{f}}
\newcommand{\bg}{\textbf{g}}
\newcommand{\bh}{\textbf{h}}
\newcommand{\bi}{\textbf{i}}
\newcommand{\bj}{\textbf{j}}
\newcommand{\bk}{\textbf{k}}
\newcommand{\bl}{\textbf{l}}
\newcommand{\bm}{\textbf{m}}
\newcommand{\bn}{\textbf{n}}
\newcommand{\bo}{\textbf{o}}
\newcommand{\bp}{\textbf{p}}
\newcommand{\bq}{\textbf{q}}
\newcommand{\br}{\textbf{r}}
\newcommand{\bs}{\textbf{s}}
\newcommand{\bt}{\textbf{t}}
\newcommand{\bu}{\textbf{u}}
\newcommand{\bv}{\textbf{v}}
\newcommand{\bw}{\textbf{w}}
\newcommand{\bx}{\textbf{x}}
\newcommand{\by}{\textbf{y}}
\newcommand{\bz}{\textbf{z}}
\newcommand{\bA}{\textbf{A}}
\newcommand{\bB}{\textbf{B}}
\newcommand{\bC}{\textbf{C}}
\newcommand{\bD}{\textbf{D}}
\newcommand{\bE}{\textbf{E}}
\newcommand{\bF}{\textbf{F}}
\newcommand{\bG}{\textbf{G}}
\newcommand{\bH}{\textbf{H}}
\newcommand{\bI}{\textbf{I}}
\newcommand{\bJ}{\textbf{J}}
\newcommand{\bK}{\textbf{K}}
\newcommand{\bL}{\textbf{L}}
\newcommand{\bM}{\textbf{M}}
\newcommand{\bN}{\textbf{N}}
\newcommand{\bO}{\textbf{O}}
\newcommand{\bP}{\textbf{P}}
\newcommand{\bQ}{\textbf{Q}}
\newcommand{\bR}{\textbf{R}}
\newcommand{\bS}{\textbf{S}}
\newcommand{\bT}{\textbf{T}}
\newcommand{\bU}{\textbf{U}}
\newcommand{\bV}{\textbf{V}}
\newcommand{\bW}{\textbf{W}}
\newcommand{\bX}{\textbf{X}}
\newcommand{\bY}{\textbf{Y}}
\newcommand{\bZ}{\textbf{Z}}

\newcommand{\cA}{\mathcal{A}}
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cC}{\mathcal{C}}
\newcommand{\cD}{\mathcal{D}}
\newcommand{\cE}{\mathcal{E}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\cG}{\mathcal{G}}
\newcommand{\cH}{\mathcal{H}}
\newcommand{\cI}{\mathcal{I}}
\newcommand{\cJ}{\mathcal{J}}
\newcommand{\cK}{\mathcal{K}}
\newcommand{\cL}{\mathcal{L}}
\newcommand{\cM}{\mathcal{M}}
\newcommand{\cN}{\mathcal{N}}
\newcommand{\cO}{\mathcal{O}}
\newcommand{\cP}{\mathcal{P}}
\newcommand{\cQ}{\mathcal{Q}}
\newcommand{\cR}{\mathcal{R}}
\newcommand{\cS}{\mathcal{S}}
\newcommand{\cT}{\mathcal{T}}
\newcommand{\cU}{\mathcal{U}}
\newcommand{\cV}{\mathcal{V}}
\newcommand{\cW}{\mathcal{W}}
\newcommand{\cX}{\mathcal{X}}
\newcommand{\cY}{\mathcal{Y}}
\newcommand{\cZ}{\mathcal{Z}}
